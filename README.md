# README #

Performance testing for Blis applications

### Requirements ###

* Install Python 2.7.x
* Install requests library

```
#!python

   pip install requests
```


### Running tests ###

#### Bidder ####

* clone this repository
* change to the bidder directory and edit file tBidRequestTiming.py

```
#!bash

cd bidder/
gedit tBidRequestTiming.py
```

* Look for method setUpClass and modify the test configuration as needed, e.g.


```
#!python

   <...>
   self.bidderAddress = "http://<EC2 instance IP address>:8080/rtb?auth_token="
   self.iterations = 1000
   self.logResultsToFile = False
   <...>

```