import os

class PerformanceTestResult:
   testClassName = ""
   testPointName = ""
   timeStamp = ""
   performanceResult =""

class PerformanceDataLogger:
   dataDir = "{0}/data".format(os.path.dirname(os.path.realpath(__file__)))
   reportsDir = "{0}/reports".format(os.path.dirname(os.path.realpath(__file__)))
   def logPerformanceData(self, testResult):
      # This logger writes the data as follows:
      # - Creates a directory with the name of the test class
      # - Creates a file for each testpoint in the test class
      # - In the file it is logged the timestamp and result of the testin CSV format
      testClassPath = "{0}/{1}".format(self.reportsDir,testResult.testClassName)
      if not os.path.exists(testClassPath):
         os.makedirs(testClassPath)
      fh = ''
      testPointPath = "{0}/{1}".format(testClassPath,testResult.testPointName)
      if not os.path.isfile(testPointPath):
         fh = open(testPointPath,"w")  
      else:
         fh = open(testPointPath,"a")
      fh.write("{0},{1}".format(testResult.timeStamp,testResult.duration));
      fh.close()

