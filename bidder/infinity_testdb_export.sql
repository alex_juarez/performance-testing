--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: dblink; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS dblink WITH SCHEMA public;


--
-- Name: EXTENSION dblink; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION dblink IS 'connect to other PostgreSQL databases from within a database';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


--
-- Name: pgstattuple; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgstattuple WITH SCHEMA public;


--
-- Name: EXTENSION pgstattuple; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgstattuple IS 'show tuple-level statistics';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: campaign; Type: TABLE; Schema: public; Owner: bidder_tester
--

CREATE TABLE campaign (
    campaign_id bigint NOT NULL,
    campaign_name character varying(255),
    description text,
    target_url character varying(1024) NOT NULL,
    status character varying(9),
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    customer_id bigint,
    creator_id bigint DEFAULT 0 NOT NULL,
    country_wide boolean DEFAULT false,
    target_site_app smallint DEFAULT 0,
    retargeting boolean DEFAULT false,
    wifi_only boolean DEFAULT false,
    advertiser_domain character varying(255) DEFAULT 'blismedia.com'::character varying,
    sample_image_url character varying(255) DEFAULT 'http://blismedia.com'::character varying,
    win_frequency_timeout_period integer DEFAULT 0,
    win_frequency_limit_quantity integer DEFAULT 0,
    win_frequency_limit_period integer DEFAULT 0,
    use_global_blocked_domains_list boolean DEFAULT true,
    impression_limit bigint DEFAULT 0 NOT NULL,
    click_limit bigint DEFAULT 0 NOT NULL,
    initial_status character varying(9),
    approval_status character varying(12) DEFAULT 'edited'::character varying,
    isp_campaign smallint DEFAULT 0,
    client_id bigint DEFAULT 0 NOT NULL,
    link_target character varying(7),
    enable_budget_spread boolean DEFAULT false NOT NULL,
    disable_pip_match boolean DEFAULT false NOT NULL,
    sell_price numeric(10,2) NOT NULL,
    and_retargeting_expression character varying(255),
    estimate_volume boolean DEFAULT false NOT NULL,
    organisation_targeting_type_id bigint DEFAULT 0 NOT NULL,
    sell_budget real DEFAULT 0 NOT NULL,
    sell_daily_budget real DEFAULT 0 NOT NULL,
    operator_iprange_type_target bigint DEFAULT 0,
    banner_position smallint DEFAULT 255,
    location_optimisation_enabled boolean DEFAULT false NOT NULL,
    time_zone_id bigint DEFAULT 1 NOT NULL,
    country_id bigint DEFAULT 1 NOT NULL,
    creator_user_id bigint DEFAULT 0 NOT NULL,
    footfall_analysis_enabled boolean DEFAULT false NOT NULL,
    footfall_analysis_type character varying(30) DEFAULT 'not_set'::character varying NOT NULL,
    campaign_type character varying(4) DEFAULT 'CPM'::character varying NOT NULL,
    currency_sell_budget numeric(15,2) DEFAULT 0.00 NOT NULL,
    currency_sell_price numeric(15,2) DEFAULT 0.00 NOT NULL,
    from_age bigint,
    to_age bigint,
    gender_target smallint DEFAULT 0 NOT NULL,
    booking_id bigint NOT NULL,
    is_removed boolean DEFAULT false NOT NULL,
    product_type_id bigint DEFAULT 1,
    ip_retargeting_enabled boolean DEFAULT false,
    revenue double precision DEFAULT 0,
    enable_creative_weighting boolean DEFAULT false NOT NULL,
    frequency_cap_level character varying(9) DEFAULT 'campaign'::character varying NOT NULL,
    accounting_type character varying(12) DEFAULT 'standard'::character varying NOT NULL,
    is_pmp_only boolean DEFAULT false NOT NULL,
    frequency_period character varying(9),
    frequency_quantity bigint NOT NULL,
    priority_tier bigint DEFAULT 1 NOT NULL,
    min_smart_pin_confidence real DEFAULT 0 NOT NULL,
    saved_status character varying(9),
    enable_dynamic_pricing boolean DEFAULT false NOT NULL,
    placement_id bigint,
    min_smart_pin_confidence_id bigint DEFAULT 1 NOT NULL,
    has_been_live boolean DEFAULT false NOT NULL,
    weight numeric(10,9),
    has_locked_weight boolean DEFAULT false NOT NULL,
    conversion_rate_type character varying(9),
    conversion_rate real DEFAULT 0.005,
    bid_budget_correction_factor real DEFAULT 1,
    yest_delivered_volume bigint DEFAULT 0 NOT NULL
);


ALTER TABLE campaign OWNER TO bidder_tester;

--
-- Name: campaign_banner_cr_schedule; Type: TABLE; Schema: public; Owner: bidder_tester
--

CREATE TABLE campaign_banner_cr_schedule (
    id bigint NOT NULL,
    campaign_banner_id bigint NOT NULL,
    schedule_id bigint NOT NULL
);


ALTER TABLE campaign_banner_cr_schedule OWNER TO bidder_tester;

--
-- Name: campaign_banner_cr_schedule_remote; Type: VIEW; Schema: public; Owner: bidder_tester
--

CREATE VIEW campaign_banner_cr_schedule_remote AS
 SELECT t1.id,
    t1.campaign_banner_id,
    t1.schedule_id
   FROM dblink('host=live.infinity-pg.db.blismedia.com dbname=infinity user=alex password=nobugs'::text, 'select * from campaign_banner_cr_schedule'::text) t1(id bigint, campaign_banner_id bigint, schedule_id bigint);


ALTER TABLE campaign_banner_cr_schedule_remote OWNER TO bidder_tester;

--
-- Name: campaign_budget; Type: TABLE; Schema: public; Owner: bidder_tester
--

CREATE TABLE campaign_budget (
    campaign_id bigint NOT NULL,
    bid_price numeric(10,2) NOT NULL,
    total_budget numeric(15,2) NOT NULL,
    daily_limit_static numeric(15,2) NOT NULL,
    daily_limit real DEFAULT 0,
    total_clicks integer DEFAULT 0,
    bid_price_static numeric(10,2) NOT NULL,
    currency_id bigint DEFAULT 1 NOT NULL,
    currency_total_budget numeric(15,2) NOT NULL,
    currency_daily_limit_static numeric(15,2) NOT NULL,
    currency_bid_price_static numeric(15,2) NOT NULL,
    estimated_margin numeric(15,2) NOT NULL,
    percentage_margin numeric(15,2) NOT NULL,
    impressions numeric(15,2) NOT NULL,
    sell_currency_id bigint DEFAULT 1 NOT NULL,
    buy_currency_id bigint DEFAULT 1 NOT NULL,
    impression_packet_initial_size bigint DEFAULT 100 NOT NULL,
    impression_packet_size_increment integer DEFAULT 0 NOT NULL,
    impression_packet_max_size bigint DEFAULT 100 NOT NULL,
    impression_packet_refresh_proportion real DEFAULT 0.1 NOT NULL,
    saved_bid_price numeric(10,2) DEFAULT 0.00 NOT NULL,
    total_completed_views integer DEFAULT 0
);


ALTER TABLE campaign_budget OWNER TO bidder_tester;

--
-- Name: campaign_budget_audit; Type: TABLE; Schema: public; Owner: bidder_tester
--

CREATE TABLE campaign_budget_audit (
    audit_id bigint NOT NULL,
    audit_timestamp timestamp without time zone DEFAULT now() NOT NULL,
    valid_until_timestamp timestamp without time zone,
    campaign_id bigint NOT NULL,
    bid_price numeric(10,2) NOT NULL,
    bid_price_static numeric(10,2) NOT NULL,
    total_budget real NOT NULL,
    daily_limit_static real NOT NULL,
    daily_limit real DEFAULT 0
);


ALTER TABLE campaign_budget_audit OWNER TO bidder_tester;

--
-- Name: campaign_budget_audit_remote; Type: VIEW; Schema: public; Owner: bidder_tester
--

CREATE VIEW campaign_budget_audit_remote AS
 SELECT t1.audit_id,
    t1.audit_timestamp,
    t1.valid_until_timestamp,
    t1.campaign_id,
    t1.bid_price,
    t1.bid_price_static,
    t1.total_budget,
    t1.daily_limit_static,
    t1.daily_limit
   FROM dblink('host=live.infinity-pg.db.blismedia.com dbname=infinity user=alex password=nobugs'::text, 'select * from campaign_budget_audit where campaign_id in (45847,45881,47320) limit 10'::text) t1(audit_id bigint, audit_timestamp timestamp without time zone, valid_until_timestamp timestamp without time zone, campaign_id bigint, bid_price numeric(10,2), bid_price_static numeric(10,2), total_budget real, daily_limit_static real, daily_limit real);


ALTER TABLE campaign_budget_audit_remote OWNER TO bidder_tester;

--
-- Name: campaign_budget_remote; Type: VIEW; Schema: public; Owner: bidder_tester
--

CREATE VIEW campaign_budget_remote AS
 SELECT t1.campaign_id,
    t1.bid_price,
    t1.total_budget,
    t1.daily_limit_static,
    t1.daily_limit,
    t1.total_clicks,
    t1.bid_price_static,
    t1.currency_id,
    t1.currency_total_budget,
    t1.currency_daily_limit_static,
    t1.currency_bid_price_static,
    t1.estimated_margin,
    t1.percentage_margin,
    t1.impressions,
    t1.sell_currency_id,
    t1.buy_currency_id,
    t1.impression_packet_initial_size,
    t1.impression_packet_size_increment,
    t1.impression_packet_max_size,
    t1.impression_packet_refresh_proportion,
    t1.saved_bid_price,
    t1.total_completed_views
   FROM dblink('host=live.infinity-pg.db.blismedia.com dbname=infinity user=alex password=nobugs'::text, 'select * from campaign_budget where campaign_id in (45847,45881,47320) limit 100'::text) t1(campaign_id bigint, bid_price numeric(10,2), total_budget numeric(15,2), daily_limit_static numeric(15,2), daily_limit real, total_clicks integer, bid_price_static numeric(10,2), currency_id bigint, currency_total_budget numeric(15,2), currency_daily_limit_static numeric(15,2), currency_bid_price_static numeric(15,2), estimated_margin numeric(15,2), percentage_margin numeric(15,2), impressions numeric(15,2), sell_currency_id bigint, buy_currency_id bigint, impression_packet_initial_size bigint, impression_packet_size_increment integer, impression_packet_max_size bigint, impression_packet_refresh_proportion real, saved_bid_price numeric(10,2), total_completed_views integer);


ALTER TABLE campaign_budget_remote OWNER TO bidder_tester;

--
-- Name: campaign_remote; Type: VIEW; Schema: public; Owner: bidder_tester
--

CREATE VIEW campaign_remote AS
 SELECT t1.campaign_id,
    t1.campaign_name,
    t1.description,
    t1.target_url,
    t1.status,
    t1.start_date,
    t1.end_date,
    t1.customer_id,
    t1.creator_id,
    t1.country_wide,
    t1.target_site_app,
    t1.retargeting,
    t1.wifi_only,
    t1.advertiser_domain,
    t1.sample_image_url,
    t1.win_frequency_timeout_period,
    t1.win_frequency_limit_quantity,
    t1.win_frequency_limit_period,
    t1.use_global_blocked_domains_list,
    t1.impression_limit,
    t1.click_limit,
    t1.initial_status,
    t1.approval_status,
    t1.isp_campaign,
    t1.client_id,
    t1.link_target,
    t1.enable_budget_spread,
    t1.disable_pip_match,
    t1.sell_price,
    t1.and_retargeting_expression,
    t1.estimate_volume,
    t1.organisation_targeting_type_id,
    t1.sell_budget,
    t1.sell_daily_budget,
    t1.operator_iprange_type_target,
    t1.banner_position,
    t1.location_optimisation_enabled,
    t1.time_zone_id,
    t1.country_id,
    t1.creator_user_id,
    t1.footfall_analysis_enabled,
    t1.footfall_analysis_type,
    t1.campaign_type,
    t1.currency_sell_budget,
    t1.currency_sell_price,
    t1.from_age,
    t1.to_age,
    t1.gender_target,
    t1.booking_id,
    t1.is_removed,
    t1.product_type_id,
    t1.ip_retargeting_enabled,
    t1.revenue,
    t1.enable_creative_weighting,
    t1.frequency_cap_level,
    t1.accounting_type,
    t1.is_pmp_only,
    t1.frequency_period,
    t1.frequency_quantity,
    t1.priority_tier,
    t1.min_smart_pin_confidence,
    t1.saved_status,
    t1.enable_dynamic_pricing,
    t1.placement_id,
    t1.min_smart_pin_confidence_id,
    t1.has_been_live
   FROM dblink('host=live.infinity-pg.db.blismedia.com dbname=infinity user=alex password=nobugs'::text, 'select * from campaign'::text) t1(campaign_id bigint, campaign_name character varying(255), description text, target_url character varying(1024), status character varying(9), start_date timestamp without time zone, end_date timestamp without time zone, customer_id bigint, creator_id bigint, country_wide boolean, target_site_app smallint, retargeting boolean, wifi_only boolean, advertiser_domain character varying(255), sample_image_url character varying(255), win_frequency_timeout_period integer, win_frequency_limit_quantity integer, win_frequency_limit_period integer, use_global_blocked_domains_list boolean, impression_limit bigint, click_limit bigint, initial_status character varying(9), approval_status character varying(12), isp_campaign smallint, client_id bigint, link_target character varying(7), enable_budget_spread boolean, disable_pip_match boolean, sell_price numeric(10,2), and_retargeting_expression character varying(255), estimate_volume boolean, organisation_targeting_type_id bigint, sell_budget real, sell_daily_budget real, operator_iprange_type_target bigint, banner_position smallint, location_optimisation_enabled boolean, time_zone_id bigint, country_id bigint, creator_user_id bigint, footfall_analysis_enabled boolean, footfall_analysis_type character varying(30), campaign_type character varying(4), currency_sell_budget numeric(15,2), currency_sell_price numeric(15,2), from_age bigint, to_age bigint, gender_target smallint, booking_id bigint, is_removed boolean, product_type_id bigint, ip_retargeting_enabled boolean, revenue double precision, enable_creative_weighting boolean, frequency_cap_level character varying(9), accounting_type character varying(12), is_pmp_only boolean, frequency_period character varying(9), frequency_quantity bigint, priority_tier bigint, min_smart_pin_confidence real, saved_status character varying(9), enable_dynamic_pricing boolean, placement_id bigint, min_smart_pin_confidence_id bigint, has_been_live boolean);


ALTER TABLE campaign_remote OWNER TO bidder_tester;

--
-- Name: campaign_version_charges; Type: TABLE; Schema: public; Owner: bidder_tester
--

CREATE TABLE campaign_version_charges (
    campaign_id bigint,
    version bigint,
    standard_media_charge_percentage numeric(4,1),
    sector_data_charge_fixed numeric(10,2),
    sector_data_charge_percentage numeric(4,1),
    transparent_location_charge_fixed numeric(10,2),
    transparent_location_charge_percentage numeric(4,1),
    location_retargeting_charge_percentage numeric(4,1),
    auction_model integer
);


ALTER TABLE campaign_version_charges OWNER TO bidder_tester;

--
-- Name: campaign_version_charges_remote; Type: VIEW; Schema: public; Owner: bidder_tester
--

CREATE VIEW campaign_version_charges_remote AS
 SELECT t1.campaign_id,
    t1.version,
    t1.standard_media_charge_percentage,
    t1.sector_data_charge_fixed,
    t1.sector_data_charge_percentage,
    t1.transparent_location_charge_fixed,
    t1.transparent_location_charge_percentage,
    t1.location_retargeting_charge_percentage,
    t1.auction_model
   FROM dblink('host=live.infinity-pg.db.blismedia.com dbname=infinity user=alex password=nobugs'::text, 'select * from campaign_version_charges where campaign_id in (45847,45881,47320) limit 10'::text) t1(campaign_id bigint, version bigint, standard_media_charge_percentage numeric(4,1), sector_data_charge_fixed numeric(10,2), sector_data_charge_percentage numeric(4,1), transparent_location_charge_fixed numeric(10,2), transparent_location_charge_percentage numeric(4,1), location_retargeting_charge_percentage numeric(4,1), auction_model integer);


ALTER TABLE campaign_version_charges_remote OWNER TO bidder_tester;

--
-- Data for Name: campaign; Type: TABLE DATA; Schema: public; Owner: bidder_tester
--

INSERT INTO campaign (campaign_id, campaign_name, description, target_url, status, start_date, end_date, customer_id, creator_id, country_wide, target_site_app, retargeting, wifi_only, advertiser_domain, sample_image_url, win_frequency_timeout_period, win_frequency_limit_quantity, win_frequency_limit_period, use_global_blocked_domains_list, impression_limit, click_limit, initial_status, approval_status, isp_campaign, client_id, link_target, enable_budget_spread, disable_pip_match, sell_price, and_retargeting_expression, estimate_volume, organisation_targeting_type_id, sell_budget, sell_daily_budget, operator_iprange_type_target, banner_position, location_optimisation_enabled, time_zone_id, country_id, creator_user_id, footfall_analysis_enabled, footfall_analysis_type, campaign_type, currency_sell_budget, currency_sell_price, from_age, to_age, gender_target, booking_id, is_removed, product_type_id, ip_retargeting_enabled, revenue, enable_creative_weighting, frequency_cap_level, accounting_type, is_pmp_only, frequency_period, frequency_quantity, priority_tier, min_smart_pin_confidence, saved_status, enable_dynamic_pricing, placement_id, min_smart_pin_confidence_id, has_been_live, weight, has_locked_weight, conversion_rate_type, conversion_rate, bid_budget_correction_factor, yest_delivered_volume) VALUES (47576, 'DegreeWomen_USA_Proximity_Day_300x250', NULL, 'http://www.degreedeodorant.com/', 'active', '2017-01-20 05:00:00', '2017-05-09 04:59:59', NULL, 72, false, 0, false, false, 'degreedeodorant.com', 'http://blismedia.com', 0, 0, 0, true, 5512, 0, 'active', 'approved', 0, 0, NULL, false, false, 10.00, NULL, false, 0, 19999.5996, 0, 0, 255, false, 370, 0, 287, false, 'not_set', 'CPM', 19999.58, 10.00, NULL, NULL, 2, 12252, false, 1, false, 21951.5299999931012, false, 'campaign', 'standard', false, 'day', 10, 1, 0, NULL, true, 13936, 1, true, 0.499989375, false, 'Manual', 1, 1, 0);
INSERT INTO campaign (campaign_id, campaign_name, description, target_url, status, start_date, end_date, customer_id, creator_id, country_wide, target_site_app, retargeting, wifi_only, advertiser_domain, sample_image_url, win_frequency_timeout_period, win_frequency_limit_quantity, win_frequency_limit_period, use_global_blocked_domains_list, impression_limit, click_limit, initial_status, approval_status, isp_campaign, client_id, link_target, enable_budget_spread, disable_pip_match, sell_price, and_retargeting_expression, estimate_volume, organisation_targeting_type_id, sell_budget, sell_daily_budget, operator_iprange_type_target, banner_position, location_optimisation_enabled, time_zone_id, country_id, creator_user_id, footfall_analysis_enabled, footfall_analysis_type, campaign_type, currency_sell_budget, currency_sell_price, from_age, to_age, gender_target, booking_id, is_removed, product_type_id, ip_retargeting_enabled, revenue, enable_creative_weighting, frequency_cap_level, accounting_type, is_pmp_only, frequency_period, frequency_quantity, priority_tier, min_smart_pin_confidence, saved_status, enable_dynamic_pricing, placement_id, min_smart_pin_confidence_id, has_been_live, weight, has_locked_weight, conversion_rate_type, conversion_rate, bid_budget_correction_factor, yest_delivered_volume) VALUES (47578, 'DegreeWomen_USA_Path_Day_300x250', NULL, 'http://www.degreedeodorant.com/', 'active', '2017-01-20 05:00:00', '2017-05-08 23:59:59', NULL, 72, true, 0, false, false, 'degreedeodorant.com', 'http://blismedia.com', 0, 0, 0, true, 567130, 0, 'active', 'approved', 0, 0, NULL, false, false, 15.00, '5513 or 5449', false, 0, 90000, 0, 0, 255, false, 370, 0, 287, false, 'not_set', 'CPM', 90000.00, 15.00, NULL, NULL, 2, 12252, false, 3, false, 49373.9250000005995, false, 'campaign', 'standard', false, 'day', 10, 1, 0, NULL, true, 13937, 1, true, 1.000000000, false, 'Manual', 1, 1, 169081);


--
-- Data for Name: campaign_banner_cr_schedule; Type: TABLE DATA; Schema: public; Owner: bidder_tester
--



--
-- Data for Name: campaign_budget; Type: TABLE DATA; Schema: public; Owner: bidder_tester
--

INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (45847, 8.00, 35.43, 4.30, 3.57628011e-07, 0, 8.00, 1, 35.43, 4.30, 8.00, 0.00, 30.00, 16670.00, 4, 1, 100, 0, 100, 0.100000001, 0.00, 0);
INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (49025, 2.00, 33.10, 5.60, 2.01399994, 0, 2.00, 1, 33.10, 5.60, 2.00, 0.00, 80.00, 31250.00, 2, 1, 100, 0, 100, 0.100000001, 0.00, NULL);
INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (45881, 2.00, 282.90, 35.00, 35.1899986, 0, 2.00, 1, 282.90, 35.00, 2.00, 0.00, 89.00, 90000.00, 2, 1, 100, 0, 100, 0.100000001, 0.00, 0);
INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (47320, 2.00, 1934.40, 25.00, 25.2000008, 0, 2.00, 1, 1934.40, 25.00, 2.00, 0.00, 89.00, 69440.00, 2, 1, 100, 0, 100, 0.100000001, 0.00, 0);
INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (47576, 3.26, 100000.00, 17.91, 1639.78394, 0, 3.25, 1, 100000.00, 17.91, 3.25, 0.00, 67.50, 0.00, 1, 1, 100, 0, 100, 0.100000001, 0.00, 0);
INSERT INTO campaign_budget (campaign_id, bid_price, total_budget, daily_limit_static, daily_limit, total_clicks, bid_price_static, currency_id, currency_total_budget, currency_daily_limit_static, currency_bid_price_static, estimated_margin, percentage_margin, impressions, sell_currency_id, buy_currency_id, impression_packet_initial_size, impression_packet_size_increment, impression_packet_max_size, impression_packet_refresh_proportion, saved_bid_price, total_completed_views) VALUES (47578, 3.27, 100001.00, 1843.17, 1483.80298, 0, 3.25, 1, 100001.00, 1843.17, 3.25, 0.00, 78.33, 0.00, 1, 1, 100, 0, 100, 0.100000001, 0.00, 0);


--
-- Data for Name: campaign_budget_audit; Type: TABLE DATA; Schema: public; Owner: bidder_tester
--

INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631417465, '2016-12-22 01:00:05.239434', NULL, 45881, 1.80, 2.00, 410.329987, 35, 33.3600006);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631385358, '2016-12-22 00:00:02.752752', NULL, 45881, 2.00, 2.00, 410.329987, 35, 34.7999992);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631383241, '2016-12-22 00:00:01.584979', NULL, 45881, 2.00, 2.00, 413.089996, 35, 32.2439003);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631383242, '2016-12-22 00:00:01.584979', NULL, 45881, 2.00, 2.00, 445.329987, 35, 0);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631383243, '2016-12-22 00:00:01.584979', NULL, 45881, 2.00, 2.00, 410.329987, 35, 35);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631524320, '2016-12-22 05:20:00.94733', NULL, 45881, 1.80, 2.00, 410.329987, 35, 27.0599995);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631474664, '2016-12-22 02:43:45.0779', NULL, 45881, 1.80, 2.00, 410.329987, 35, 30.8400002);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631520017, '2016-12-22 05:04:32.196766', NULL, 45881, 1.80, 2.00, 410.329987, 35, 27.4200001);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631504317, '2016-12-22 04:05:20.935131', NULL, 45881, 1.80, 2.00, 410.329987, 35, 28.8600006);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (631505299, '2016-12-22 04:16:11.873252', NULL, 45881, 1.80, 2.00, 410.329987, 35, 28.6800003);
INSERT INTO campaign_budget_audit (audit_id, audit_timestamp, valid_until_timestamp, campaign_id, bid_price, bid_price_static, total_budget, daily_limit_static, daily_limit) VALUES (647720923, '2017-02-06 00:00:01.869513', NULL, 49025, 2.00, 2.00, 33.0999985, 4.19999981, 4.19399977);


--
-- Data for Name: campaign_version_charges; Type: TABLE DATA; Schema: public; Owner: bidder_tester
--

INSERT INTO campaign_version_charges (campaign_id, version, standard_media_charge_percentage, sector_data_charge_fixed, sector_data_charge_percentage, transparent_location_charge_fixed, transparent_location_charge_percentage, location_retargeting_charge_percentage, auction_model) VALUES (45847, 1, 0.0, NULL, NULL, 0.00, NULL, 0.0, 0);
INSERT INTO campaign_version_charges (campaign_id, version, standard_media_charge_percentage, sector_data_charge_fixed, sector_data_charge_percentage, transparent_location_charge_fixed, transparent_location_charge_percentage, location_retargeting_charge_percentage, auction_model) VALUES (47576, 1, 0.0, NULL, NULL, 0.00, NULL, 0.0, 0);
INSERT INTO campaign_version_charges (campaign_id, version, standard_media_charge_percentage, sector_data_charge_fixed, sector_data_charge_percentage, transparent_location_charge_fixed, transparent_location_charge_percentage, location_retargeting_charge_percentage, auction_model) VALUES (47578, 1, 0.0, NULL, NULL, NULL, NULL, 0.0, 0);
INSERT INTO campaign_version_charges (campaign_id, version, standard_media_charge_percentage, sector_data_charge_fixed, sector_data_charge_percentage, transparent_location_charge_fixed, transparent_location_charge_percentage, location_retargeting_charge_percentage, auction_model) VALUES (47578, 2, 0.0, NULL, NULL, 0.00, NULL, 0.0, 0);
INSERT INTO campaign_version_charges (campaign_id, version, standard_media_charge_percentage, sector_data_charge_fixed, sector_data_charge_percentage, transparent_location_charge_fixed, transparent_location_charge_percentage, location_retargeting_charge_percentage, auction_model) VALUES (47578, 3, 0.0, NULL, NULL, NULL, NULL, 0.0, 0);


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: campaign idx_campaign_campaign_id_pkey; Type: CONSTRAINT; Schema: public; Owner: bidder_tester
--

ALTER TABLE ONLY campaign
    ADD CONSTRAINT idx_campaign_campaign_id_pkey PRIMARY KEY (campaign_id);


--
-- PostgreSQL database dump complete
--

