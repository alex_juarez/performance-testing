# System imports
import requests
import os
import unittest
import json
import time
import math
import collections

# Local imports
from Utils import PerformanceTestResult, PerformanceDataLogger

class BidderPerformanceTester(unittest.TestCase):

   # Class setup and teardown
   @classmethod
   def setUpClass(self):
      self.dataDir = "{0}/data".format(os.path.dirname(os.path.realpath(__file__)))
      #self.bidderAddress = "http://10.91.30.45:8080/rtb?auth_token="
      self.bidderAddress = "http://10.92.31.229:8080/rtb?auth_token="
      self.dataLogger = PerformanceDataLogger()
      self.iterations = 2000
      self.logResultsToFile = False
      self.testTimestamp = time.time()

   # Helper methods
   def hPostHttpRequest(self, ssp_auth_token, requestJson):     
      # Execute the post request and return the result
      bUrl = self.bidderAddress+ssp_auth_token
      return requests.post(bUrl,json=requestJson)

   def hGetRequestJsonFromFile(self, requestFilename):
      # Get the contents of the bid request and transform it into a JSON object
      jsonBidRequestData = ""
      with open(requestFilename, 'r') as content_file:
          jsonBidRequestData = content_file.read()
      return json.loads(jsonBidRequestData)

   def hGetPerformanceResults(self, perfData):
      # For performance data we eliminate the shortest and longest duration to 
      # prevent skewed results. Then we average the duration of the remaining 
      # measurements. Return results as float
      perfData.sort()
      if(len(perfData)>4):
         del perfData[0]
         del perfData[-1]
      PerformanceResult = collections.namedtuple("PerformanceResult",["numRequests","duration"])
      return PerformanceResult(len(perfData),sum(perfData)/float(len(perfData)))

   def hLogResults(self, tId, pResult):     
      testIdArray= tId.split(".");
      testResult = PerformanceTestResult()
      testResult.testClassName = testIdArray[1]
      testResult.testPointName = testIdArray[2]
      testResult.timeStamp = self.testTimestamp
      testResult.numRequests = pResult.numRequests
      testResult.duration = pResult.duration
      print("Test: {0} No. of issued requests: {1} Average execution time: {2}".format(testResult.testPointName, testResult.numRequests, testResult.duration))
      if(self.logResultsToFile):
         self.dataLogger.logPerformanceData(testResult)

   # Testcases 
   def testMeasureRejectedBidRequests(self):
      # We use a single SSP  and bid request for this test case
      bidRequestFilename = self.dataDir+"/FailedBidRequest.json"
      jData = self.hGetRequestJsonFromFile(bidRequestFilename)
      auth_token = "634ee88a060fc90a807df12a7c521f4f"
      bidRequestsDuration = []
      for b in range(0,self.iterations):
         r = self.hPostHttpRequest(auth_token, jData); 
         bidRequestsDuration.append(r.elapsed.total_seconds())
      # Finally log result         
      testId = self.id()
      perfResult = self.hGetPerformanceResults(bidRequestsDuration)
      self.hLogResults(testId, perfResult)

   def testMeasureAcceptedBidRequests(self):
      # We use a single SSP  and bid request for this test case
      bidRequestFilename = self.dataDir+"/SuccessfulBidRequest.json"
      jData = self.hGetRequestJsonFromFile(bidRequestFilename)
      auth_token = "634ee88a060fc90a807df12a7c521f4f"
      bidRequestsDuration = []
      for b in range(0,self.iterations):
         r = self.hPostHttpRequest(auth_token, jData); 
         bidRequestsDuration.append(r.elapsed.total_seconds())
      # Finally log result         
      testId = self.id()
      perfResult = self.hGetPerformanceResults(bidRequestsDuration)
      self.hLogResults(testId, perfResult)

   def testMeasureMixedBidRequests(self):
      # We use a single SSP  and two bid requests alternating for this test case
      goodBidRequestFilename = self.dataDir+"/SuccessfulBidRequest.json"
      badBidRequestFilename = self.dataDir+"/FailedBidRequest.json"
      jGoodData = self.hGetRequestJsonFromFile(goodBidRequestFilename)
      jBadData = self.hGetRequestJsonFromFile(badBidRequestFilename)
      auth_token = "634ee88a060fc90a807df12a7c521f4f"
      bidRequestsDuration = []
      limit = int(math.ceil(self.iterations/2))
      for b in range(0,limit):
         r = self.hPostHttpRequest(auth_token, jGoodData); 
         bidRequestsDuration.append(r.elapsed.total_seconds())
         r = self.hPostHttpRequest(auth_token, jBadData); 
         bidRequestsDuration.append(r.elapsed.total_seconds())
      # Finally log result         
      testId = self.id()
      perfResult = self.hGetPerformanceResults(bidRequestsDuration)
      self.hLogResults(testId, perfResult)

if __name__ == '__main__':
   unittest.main()
